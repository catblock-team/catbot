#! /usr/bin/python2
# -*- coding: utf-8 -*-
u"""
Bot designed for running on catblock.slack.com, our Slack server.

This is the main file for the package.

Copyright © 2016 Kieran Peckett.
This file is part of the CatBlock Slack Bot.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import debug
import exceptions
import message
import settings
from slackclient import SlackClient
from time import sleep

slack = SlackClient(settings.get("API_KEY"))


def printCopyrightInfo():
    """Print the copyright info for the program."""
    print ("The CatBlock Bot  Copyright (C) 2016  Kieran ")
    print ("This program comes with ABSOLUTELY NO WARRANTY; for details")
    print ("PM the bot with \".warranty\".")
    print ("This is free software, and you are welcome to redistribute it")
    print ("under certain conditions; PM the bot with \".redist\" for details.")


def test_connection():
    """Test the connection to make sure we're connected properly."""
    global slack
    test_data = slack.api_call("api.test")
    debug.consoleLog("API Test", test_data)
    if test_data["ok"] is False:
        raise exceptions.ConnectionError("Main API")
        return False
    return True


def rtm_begin():
    """Start the connection to the Real Time Messaging API."""
    global slack, debug
    rtm = slack.rtm_connect()
    debug.consoleLog("RTM Connect", rtm)
    if rtm:
        print ("Connected to RTM API. The bot is ready.")
        return True
    else:
        raise exceptions.ExceptionError("RTM")


def rtm_event_check():
    """Main function of the bot. Setting time_to_quit to False ends the bot."""
    time_to_quit = False
    while not time_to_quit:
        new_events = slack.rtm_read()
        for event in new_events:
            debug.consoleLog("New Event", event)
            if "type" in event:
                run_event(event)
        sleep(1)


def run_event(event):
    """Take an event and connect it to its relevant helper function.

    Args:
        event: (dict) event dict provided by the API
    """
    Type = event["type"]
    if Type == "hello":
        print ("Recieved connection confirmation to RTM API.")
    elif Type == "message":
        message.log(event)
        message.checkRespond(slack, event)


def main():
    connected = test_connection()
    if connected:
        print ("Connected to Slack API. Establishing RTM connection.")
        if rtm_begin():
            rtm_event_check()
    else:
        debug.consoleLog("Connection error", connected)

if __name__ == '__main__':
    main()
