# -*- coding: utf-8 -*-
u"""
Debugging Functions. Change "DEBUG_ON" to True to enable debugging.

Please note that enabling debugging will slow down the program.

Copyright © 2016 Kieran Peckett.
This file is part of the CatBlock Slack Bot.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime
from pprint import pformat
from pprint import pprint

DEBUG_ON = True

if DEBUG_ON:
    print ("Debugging Enabled.")


def consoleLog(name, message):
    """Log message input to the console using PrettyPrint.

    Args:
        name: (str) A 1-2 word description of the debug info being dumped.
        message: (any) The data to be pretty-printed.
    """
    if DEBUG_ON:
        print ("Debug: " + name)
        pprint(message)
        with open("log.txt", "a+") as logfile:
            logfile.write(datetime.now().isoformat(" ") + " " + pformat(message) + "\n")
