# -*- coding: utf-8 -*-
u"""
Exceptions designed to be raised by the program.

Copyright © 2016 Kieran Peckett.
This file is part of the CatBlock Slack Bot.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


class ConnectionError(Exception):
    """There was a problem when connecting to (type)"""
    def __init__(self, type):
        self.connection_type = type

    def __str__(self):
        return "Error connecting to:" + repr(self.connection_type)
