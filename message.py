# -*- coding: utf-8 -*-
u"""
Manages message events.

Copyright © 2016 Kieran Peckett.
This file is part of the CatBlock Slack Bot.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime
import debug
import messageHandlers
import settings


def respond(slack, user_id, response, channel_id):
    """Send a message to a user as a reply.

    Args:
        slack: (SlackClient) the slack client object
        user_id: (str) the ID of the user provided by the slack API.
        response: (str) the message to send to the user
        channel_id: (str) the ID of the channel to send the message to
    """
    user_name = slack.api_call("users.info", user=user_id)["user"]["name"]
    message = '@' + user_name + ' ' + response
    send(slack, message, channel_id)


def send(slack, message, channel_id):
    """Send a message to a channel, but not as a reply.

    Args:
        slack: (SlackClient) the slack client object
        message: (str) the message you wish to send
        channel_id: (str) the ID of the channel you wish to send to
    """
    debug.consoleLog("Sending Message", {"channel": channel_id, "message": message})
    slack.rtm_send_message(channel_id, message)


def checkRespond(slack, event):
    """Check if the bot needs to respond to a message.

    Args:
        slack: (SlackClient) the slack client object
        event: (dict) event dict from the Slack API
    """
    if event["text"][0] == command_character:
        for command in commands:
            if (event["text"] == command_character + command) or\
                  event["text"].startswith(command_character + command + " "):
                commands[command](slack, event)
            else:
                debug.consoleLog("Command is not", command)


def log(slack, event):
    """Take a message event and log it to the relevant file for that channel.

    Args:
        slack: (SlackClient) the slack client object
        event: (dict) event dict from the Slack API
    """
    message_time = datetime.utcfromtimestamp(int(float(event["ts"])))
    user_info = slack.api_call("users.info", user=event["user"])
    user_name = user_info["user"]["name"]
    channel_id = event["channel"]
    if channel_id[0] == 'C':
        # public channel
        # TODO(Kieran) use custom func for API call to check ok==false
        channel_type = "channels"
        channel_info = slack.api_call("channels.info", channel=channel_id)
        channel_name = channel_info["channel"]["name"]
    elif channel_id[0] == 'G':
        # private channel
        channel_type = "groups"
        channel_info = slack.api_call("groups.info", channel=channel_id)
        channel_name = channel_info["group"]["name"]
    elif channel_id[0] == "D":
        # direct message to bot
        # assume user sending is the chat name
        channel_type = "ims"
        channel_name = user_name
    else:
        channel_type = "unknown"
        channel_name = channel_id
    with open("logs/" + channel_type + '/' + channel_name + ".txt", 'a+') \
                 as log_file:
        log_file.write(message_time.isoformat(" ") + " <" + user_name + ">: " +
                       event["text"] + "\n")

command_character = settings.get("command_character")
commands = {
    "warranty": messageHandlers.sendWarrantyInfo,
    "redist": messageHandlers.sendRedistributionInfo,
    "redistribute": messageHandlers.sendRedistributionInfo,
    "help": messageHandlers.listCommands,
    "commands": messageHandlers.listCommands
}
