# -*- coding: utf-8 -*- 
u"""
Functions to handle messages coming in with commands.

Copyright © 2016 Kieran Peckett.
This file is part of the CatBlock Slack Bot.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import copyright
import message


def sendWarrantyInfo(slack, event):
    message.respond(slack, event["user"], copyright.warranty_info(), event["channel"])


def sendRedistributionInfo(slack, event):
    message.respond(slack, event["user"], copyright.redistribute_info(), event["channel"])


def listCommands(slack, event):
    messageArray = []
    for command in message.commands:
        messageArray.append(message.command_character + command)
    message.respond(slack, event["user"], ", ".join(messageArray), event["channel"])
