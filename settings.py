# -*- coding: utf-8 -*-
u"""
Functions to handle getting settings from settings.json.

Copyright © 2016 Kieran Peckett.
This file is part of the CatBlock Slack Bot.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import json


def get(setting_id):
    """Get a setting from the settings.json file."""
    with open("settings.json", "rU") as settings_file:
        settings = json.load(settings_file)
    return settings[setting_id]


def set(setting_id, setting):
    """Set a setting in the settings.json file."""
    with open("settings.json", "rU") as settings_file:
        settings = json.load(settings_file)
    settings[setting_id] = setting
    with open("settings.json", "w") as settings_file:
        json.dump(settings, settings_file, ensure_ascii=False, indent=4, sort_keys=True)
